---
layout: post
title: Welcome To S1 News India
categories: [news, maroli]
---

![S1 News India](https://gitlab.com/s1newsindia/forever-jekyll/-/raw/main/assets/image/s1news-studio.JPG "S1 News India")  

## About Us
We are an independent news outlet based in Maroli, Gujarat, India.  
We keep a keen eye on all the happenings in Maroli and nearby coastal villages. We also closely follow news and events happening in Navsari district and other parts of south Gujarat. Though our primary objective is to deliver news from in and around southern part of Gujarat, we also deliver important international and national news to our viewers.  
You can follow S1 News India on YouTube and Facebook for all the latest news. And yes, please don't forget to like, share and subscribe.  

<i class="fa fa-youtube" aria-hidden="true"></i> [S1 News India's YouTube Channel](https://www.youtube.com/c/S1NEWSINDIA){:target="_blank"}  
<i class="fa fa-facebook-official" aria-hidden="true"></i> [S1 News India's Facebook Page](https://www.facebook.com/s1newsindia/){:target="_blank"}  
<br>
<hr>
<div class="latest_video">
<iframe class="latestVideoEmbed" vnum='0' cid="UCKo7h12_NDGDfd1BgornFoA" width="600" height="340" frameborder="0" allowfullscreen></iframe>
<p id="video-title">Lorem ipsum</p>
</div>
<br>
<hr>
![Shmsher Kilaniya with Shri C. R. Patil](https://gitlab.com/s1newsindia/forever-jekyll/-/raw/main/assets/image/FB_IMG_1661775921971.jpg "Shmsher Kilaniya with Shri C. R. Patil")

![Shmsher Kilaniya](https://gitlab.com/s1newsindia/forever-jekyll/-/raw/9ecee291a03986b67908e001191e94656e9d4bfa/assets/image/20220822_122519.jpg "Shmsher Kilaniya")

![Suresh Boliwal](https://gitlab.com/s1newsindia/forever-jekyll/-/raw/9ecee291a03986b67908e001191e94656e9d4bfa/assets/image/IMG-20220822-WA0003.jpg "Suresh Boliwal")
<br>
<hr>
## Our Team
<i class="fa fa-id-card" aria-hidden="true"></i> Shmsher Kilaniya  
 - Post - Editor-in-chief  
 - Contact - [+917878007300](tel:+917878007300)  
 - Email - [talvar786@gmail.com](mailto:talvar786@gmail.com)   
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Suresh Boliwal  
 - Post - Editor  
 - Contact - [ +919574215555](tel:+919574215555)  
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Yogesh Patel  
 - Post - Video Journalist  
 - Contact - [+919662634663](tel:+919662634663)  
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Mehboob Kayat  
 - Post - Journalist  
 - Contact - [+919925472715](tel:+919925472715)  
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Gautam Lalvani  
 - Post - Journalist  
 - Contact - [+919825881668](tel:+919825881668)  
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Aslam Najir Rangrej  
 - Post - Journalist  
 - Contact - [+919924685490](tel:+919924685490)  
<br>

<i class="fa fa-id-card" aria-hidden="true"></i> Bhakti Lalvani  
 - Post - News Anchor  
 - Contact - [+919724876311](tel:+919724876311)  
<br>

<hr>
## Advertise With Us
If you want to promote your goods, products, services in and around Nvsari district and south Gujarat then we can be your perfect partners. Advertising with us is not only cost effective but it also will help you reach out to thousands of our viewers. For further information, please feel free to contact us.  
<br>
<hr>
## Contact Us
<i class="fa fa-building" aria-hidden="true"></i> Office - Janta Society, Chhinam Road, Maroli, Gujarat, India.  
<i class="fa fa-phone" aria-hidden="true"></i> Phone Line 1 - [7878007300](tel:7878007300)  
<i class="fa fa-phone" aria-hidden="true"></i> Phone Line 2 - [9574215555](tel:9574215555)  
<i class="fa fa-envelope" aria-hidden="true"></i> Email - [s1news007@gmail.com](mailto:s1news007@gmail.com)  
<br>
